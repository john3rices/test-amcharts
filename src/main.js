import * as am4charts from "@amcharts/amcharts4/charts";
import * as am4core from "@amcharts/amcharts4/core";
import escapeHTML from "escape-html";

am4core.ready(function () {
  // Create chart instance
  var chart = am4core.create("chartdiv", am4charts.XYChart);

  // Add data
  chart.data = [
    { date: new Date(2011, 0, 1), value: 3000 },
    { date: new Date(2012, 0, 1), value: 4000 },
    { date: new Date(2013, 0, 1), value: 4000 },
    { date: new Date(2014, 0, 1), value: 5000 },
    { date: new Date(2015, 0, 1), value: 5000 },
    { date: new Date(2016, 0, 1), value: 6000 },
    { date: new Date(2017, 0, 1), value: 6000 },
    { date: new Date(2018, 0, 1), value: 7000 },
    { date: new Date(2019, 0, 1), value: 7000 },
    { date: new Date(2020, 0, 1), value: 8000, special: true },
  ];

  // Create axes
  var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
  var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

  valueAxis.extraMax = 2;
  valueAxis.renderer.labels.template.adapter.add(
    "hidden",
    (value, target, key) => {
      return target.dataItem.getValue("value") > 8000;
    }
  );
  valueAxis.renderer.grid.template.adapter.add(
    "hidden",
    (value, target, key) => {
      return target.dataItem.getValue("value") > 8000;
    }
  );

  chart.dateFormatter = new am4core.DateFormatter();
  dateAxis.dateFormats.setKey("year", "''''yy");
  dateAxis.gridIntervals = new am4core.List([{ timeUnit: "year", count: 1 }]);
  dateAxis.renderer.labels.template.disabled = false;
  dateAxis.renderer.labels.template.fill = am4core.color("#00ff00");
  dateAxis.renderer.labels.template.adapter.add(
    "fill",
    (value, target, key) => {
      return target.dataItem.getValue("value") ===
        chart.data[chart.data.length - 1].date.getTime()
        ? am4core.color("#ff0000")
        : value;
    }
  );

  // Create series
  var series = chart.series.push(new am4charts.LineSeries());
  series.dataFields.valueY = "value";
  series.dataFields.dateX = "date";
  series.tooltipText = "{value}";
  series.strokeWidth = 2;
  series.minBulletDistance = 15;
  series.stroke = new am4core.LinearGradient();
  series.stroke.addColor(am4core.color("#ff0000"), 1, 0);
  series.stroke.addColor(am4core.color("#0000ff"), 1, 1);
  let bullet = series.bullets.push(new am4charts.Bullet());
  bullet.adapter.add("hidden", (value, target, key) => {
    return target.dataItem?.dataContext.special !== true ?? value;
  });
  let square = bullet.createChild(am4core.Rectangle);
  square.width = 10;
  square.height = 10;

  // Drop-shaped tooltips
  series.tooltip.background.cornerRadius = 20;
  series.tooltip.background.strokeOpacity = 0;
  series.tooltip.pointerOrientation = "vertical";
  series.tooltip.label.minWidth = 40;
  series.tooltip.label.minHeight = 40;
  series.tooltip.label.textAlign = "middle";

  // Make a panning cursor
  chart.cursor = new am4charts.XYCursor();
  chart.cursor.behavior = "none";
  chart.cursor.xAxis = dateAxis;
  chart.cursor.snapToSeries = series;
}); // end am4core.ready()

am4core.ready(function () {
  // Create chart instance
  var chart = am4core.create("barchartdiv", am4charts.XYChart);
  chart.logo.disabled = true;

  // Add data
  chart.data = [
    { date: new Date(2016, 0, 1), value: 400 },
    { date: new Date(2017, 0, 1), value: 450 },
    { date: new Date(2018, 0, 1), value: 500 },
    { date: new Date(2019, 0, 1), value: 550 },
    { date: new Date(2020, 0, 1), value: 600, special: true },
  ];

  // Create axes
  var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
  var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

  valueAxis.renderer.labels.template.disabled = true;

  chart.dateFormatter = new am4core.DateFormatter();
  dateAxis.dateFormats.setKey("year", "''''yy");
  dateAxis.gridIntervals = new am4core.List([{ timeUnit: "year", count: 1 }]);
  dateAxis.renderer.labels.template.disabled = false;
  dateAxis.renderer.labels.template.fill = am4core.color("#00ff00");
  dateAxis.renderer.labels.template.adapter.add(
    "fill",
    (value, target, key) => {
      return target.dataItem.getValue("value") ===
        chart.data[chart.data.length - 1].date.getTime()
        ? am4core.color("#ff0000")
        : value;
    }
  );

  // Create series
  var series = chart.series.push(new am4charts.ColumnSeries());
  series.dataFields.valueY = "value";
  series.dataFields.dateX = "date";
  series.tooltipText = "{value}";
  series.strokeWidth = 0;
  series.minBulletDistance = 0;
  series.fill = am4core.color("#999999");
  series.columns.template.adapter.add("fill", (value, target, key) => {
    const special = target.dataItem.dataContext.special ?? false;
    if (!special) return value;

    const fill = new am4core.LinearGradient();
    fill.addColor(am4core.color("#ff0000"), 1, 0);
    fill.addColor(am4core.color("#0000ff"), 1, 1);
    fill.rotation = 90;

    return fill;
  });
  let bullet = series.bullets.push(new am4charts.LabelBullet());
  bullet.adapter.add("hidden", (value, target, key) => {
    return target.dataItem.dataContext.special ?? false;
  });
  bullet.label.text = "{value}";
  bullet.label.horizontalCenter = "left";
  bullet.label.verticalCenter = "middle";
  bullet.label.rotation = 90;

  let bullet2 = series.bullets.push(new am4charts.LabelBullet());
  bullet2.adapter.add("hidden", (value, target, key) => {
    return !(target.dataItem.dataContext.special ?? false);
  });
  bullet2.label.html = "{value}";
  bullet2.label.horizontalCenter = "middle";
  bullet2.label.verticalCenter = "bottom";
  bullet2.label.adapter.add("htmlOutput", (value, target, key) => {
    return "<ul><li>" + escapeHTML(value) + "</li><li>test</li></ul>";
  });

  // Drop-shaped tooltips
  series.tooltip.background.cornerRadius = 20;
  series.tooltip.background.strokeOpacity = 0;
  series.tooltip.pointerOrientation = "vertical";
  series.tooltip.label.minWidth = 40;
  series.tooltip.label.minHeight = 40;
  series.tooltip.label.textAlign = "middle";

  // Make a panning cursor
  chart.cursor = new am4charts.XYCursor();
  chart.cursor.behavior = "none";
  chart.cursor.xAxis = dateAxis;
  chart.cursor.snapToSeries = series;
}); // end am4core.ready()
